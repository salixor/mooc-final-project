﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace MOOCFinalProject
{
    /// <summary>
    /// Provides utilities for checking collisions
    /// </summary>
    public class CollisionUtils
    {
        /// <summary>
        /// Pixel perfect collision between two entities
        /// </summary>
        /// <param name="a">the first entity</param>
        /// <param name="b">the second entity</param>
        public static bool PerPixelCollision(Entity a, Entity b)
        {
            // get Color data of each Texture
            Color[] bitsA = new Color[a.Sprite.Width * a.Sprite.Height];
            a.Sprite.GetData(bitsA);
            Color[] bitsB = new Color[b.Sprite.Width * b.Sprite.Height];
            b.Sprite.GetData(bitsB);

            // calculate the intersecting rectangle
            int top = Math.Max(a.DrawRectangle.Top, b.DrawRectangle.Top);
            int bottom = Math.Min(a.DrawRectangle.Bottom, b.DrawRectangle.Bottom);
            int left = Math.Max(a.DrawRectangle.Left, b.DrawRectangle.Left);
            int right = Math.Min(a.DrawRectangle.Right, b.DrawRectangle.Right);

            // for each single pixel in the intersecting rectangle
            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    try
                    {
                        Color colorA = bitsA[
                            (a.SourceRectangle.Left + x - a.DrawRectangle.Left)
                          + (a.SourceRectangle.Top + y - a.DrawRectangle.Top) * a.Sprite.Width
                        ];
                        Color colorB = bitsB[
                            (b.SourceRectangle.Left + x - b.DrawRectangle.Left)
                          + (b.SourceRectangle.Top + y - b.DrawRectangle.Top) * b.Sprite.Width
                        ];

                        if (colorA.A != 0 && colorB.A != 0) // if both colors are not transparent (the alpha channel is not 0), then there is a collision
                        {
                            return true;
                        }
                    } catch (Exception e) { }
                }
            }

            return false;
        }
    }
}
