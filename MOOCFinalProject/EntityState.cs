﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOOCFinalProject
{
    /// <summary>
    /// An enumeration for possible entity states
    /// </summary>
    public enum EntityState
    {
        Idle,
        Moving,
        MovingTop,
        MovingBottom,
        MovingLeft,
        MovingRight,
        MovingTopLeft,
        MovingTopRight,
        MovingBottomLeft,
        MovingBottomRight,
        Shooting
    }
}
