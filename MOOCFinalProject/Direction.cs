﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MOOCFinalProject
{
    /// <summary>
    /// An enumeration for possible directions
    /// </summary>
    public enum Direction
    {
        None,
        Top,
        Bottom,
        Left,
        Right,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight
    }
}
