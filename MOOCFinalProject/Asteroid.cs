﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOOCFinalProject
{
    /// <summary>
    /// An asteroid
    /// </summary>
    class Asteroid : Entity
    {
        #region Fields

        // random field
        Random random = new Random();

        // current damage value for the asteroid
        int damage;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new asteroid object
        /// </summary>
        /// <param name="sprite">the sprite for the asteroid</param>
        /// <param name="width">the width of the asteroid</param>
        /// <param name="height">the height of the asteroid</param>
        public Asteroid(Texture2D sprite, int width, int height)
            : base(sprite, 0, 0, width, height, Vector2.Zero, false)
        {
            damage = GameConstants.AsteroidStartDamage;
            ChangeState(EntityState.Moving);
            Velocity = GetRandomVelocity();
            DrawRectangle = new Rectangle(
                GetRandomLocation(),
                new Point(width, height)
            );
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets and sets the damage value for the asteroid
        /// </summary>
        public int Damage
        {
            get { return damage; }
            set { damage = value; }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Create a random location
        /// </summary>
        private Point GetRandomLocation()
        {
            int rand = random.Next(4);
            
            if (rand == 0)
                return new Point(random.Next(GameConstants.WindowWidth), -Height);
            if (rand == 1)
                return new Point(random.Next(GameConstants.WindowWidth), GameConstants.WindowHeight + Height);
            if (rand == 2)
                return new Point(-Width, random.Next(GameConstants.WindowHeight));
            if (rand == 3)
                return new Point(GameConstants.WindowWidth + Width, random.Next(GameConstants.WindowHeight));


            return new Point(0, 0);
        }

        /// <summary>
        /// Create a random velocity
        /// </summary>
        private Vector2 GetRandomVelocity()
        {
            float xVelocity = RandomNumberGenerator.RandomFloat(GameConstants.AsteroidMinSpeed, GameConstants.AsteroidMaxSpeed);
            float yVelocity = RandomNumberGenerator.RandomFloat(GameConstants.AsteroidMinSpeed, GameConstants.AsteroidMaxSpeed);
            float angle = RandomNumberGenerator.RandomFloat(0, Math.PI);

            return new Vector2((float)Math.Cos(angle) * xVelocity, (float)Math.Sin(angle) * yVelocity);
        }

        #endregion
    }
}
