﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace MOOCFinalProject
{
    /// <summary>
    /// All the constants used in the game
    /// </summary>
    public static class GameConstants
    {
        // resolution of the window
        public const int WindowWidth = 800;
        public const int WindowHeight = 600;
        public static readonly Point WindowOrigin = new Point(0, 0);
        public static readonly Point WindowSize = new Point(WindowWidth, WindowHeight);
        public static readonly Rectangle WindowLocation = new Rectangle(WindowOrigin, WindowSize);

        // button related constants
        public const int ButtonWidth = 400;
        public const int ButtonHeight = 100;
        public const int MenuButtonOffset = 280;
        public const int StartSoloButtonOffset = 280;
        public const int StartVersusButtonOffset = 390;
        public const int ContinueButtonOffset = 390;
        public const int QuitButtonOffset = 500;

        // character related constants
        public const int CharacterStartHealth = 100;
        public const int CharacterShootingCooldown = 500;
        public const float CharacterVelocity = 0.4f;
        public const int ScorePerHit = 25;
        public const int ScorePerAsteroid = 5;
        public const int CharacterXOffset = 200;
        public static readonly Vector2 Character1StartLocation = new Vector2(CharacterXOffset, WindowHeight / 2);
        public static readonly Vector2 Character2StartLocation = new Vector2(WindowWidth - CharacterXOffset, WindowHeight / 2);

        // projectile related constants
        public const int ProjectileStartDamage = 20;
        public const float ProjectileVelocity = 0.6f;
        public const int ProjectileWidth = 17;
        public const int ProjectileHeight = 17;

        // asteroid related constants
        public const int AsteroidStartDamage = 25;
        public const int SoloAsteroidDamage = 25;
        public const int VersusAsteroidDamage = 10;
        public const int AsteroidSize = 64;
        public const float AsteroidMinSpeed = 0.1f;
        public const float AsteroidMaxSpeed = 0.35f;
        public const int MinAsteroidCount = 5;
        public const int SoloAsteroidSpawnRate = 1500;
        public const int VersusAsteroidSpawnRate = 4000;

        // explosion related constants
        public const int ExplosionSize = 50;

        // pop up related constants
        public const int PopUpDuration = 1000;

        // text prefixes and suffixes
        public const string HealthSuffix = "HP";
        public const string ScorePrefix = "Score : ";
        public const string TotalScorePrefix = "Total score : ";

        // health related constants
        public const double HighHealthPercentage = 0.7;
        public const double MediumHealthPercentage = 0.3;

        // useful colors
        public static readonly Color GreenColor = Color.LightGreen;
        public static readonly Color OrangeColor = Color.LightYellow;
        public static readonly Color RedColor = Color.IndianRed;

        // text location
        public const int PlayerDisplayOffsetX = 20;
        public const int HealthDisplayOffsetY = 30;
        public const int ScoreDisplayOffsetY = 60;
        public const int TotalScoreDisplayOffsetY = 90;
        public static readonly Vector2 Player1HealthLocation = new Vector2(PlayerDisplayOffsetX, HealthDisplayOffsetY);
        public static readonly Vector2 Player2HealthLocation = new Vector2(WindowWidth - PlayerDisplayOffsetX, HealthDisplayOffsetY);
        public static readonly Vector2 Player1ScoreLocation = new Vector2(PlayerDisplayOffsetX, ScoreDisplayOffsetY);
        public static readonly Vector2 Player2ScoreLocation = new Vector2(WindowWidth - PlayerDisplayOffsetX, ScoreDisplayOffsetY);
        public static readonly Vector2 Player1TotalScoreLocation = new Vector2(PlayerDisplayOffsetX, TotalScoreDisplayOffsetY);
        public static readonly Vector2 Player2TotalScoreLocation = new Vector2(WindowWidth - PlayerDisplayOffsetX, TotalScoreDisplayOffsetY);

        // menu and pause texts
        public const string MenuText = "Menu";
        public const string PauseText = "Game paused !";
        public const string FinishText = "Game finished !";
        public static readonly Vector2 MenuTextLocation = new Vector2(WindowWidth / 2, 100);
        public static readonly Vector2 PauseTextLocation = new Vector2(WindowWidth / 2, 100);
        public static readonly Vector2 FinishTextLocation = new Vector2(WindowWidth / 2, 100);
    }
}
