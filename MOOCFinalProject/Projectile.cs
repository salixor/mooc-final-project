﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MOOCFinalProject
{
    /// <summary>
    /// A projectile
    /// </summary>
    public class Projectile : Entity
    {
        #region Fields

        // current damage value for the projectile
        int damage;

        // the entity that shot this projectile
        Entity shotBy;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new projectile object
        /// </summary>
        /// <param name="sprite">the already loaded sprite for the projectile</param>
        /// <param name="x">the x location of the center of the projectile</param>
        /// <param name="y">the y location of the center of the projectile</param>
        /// <param name="width">the width location of the center of the projectile</param>
        /// <param name="height">the height location of the center of the projectile</param>
        /// <param name="velocity">the initial velocity of the projectile</param>
        /// <param name="shotBy">the entity that shot this projectile</param>
        public Projectile(Texture2D sprite, int x, int y, int width, int height,
            Vector2 velocity, Entity shotBy)
            : base(sprite, x, y, width, height, velocity, false)
        {
            damage = GameConstants.ProjectileStartDamage;
            this.shotBy = shotBy;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets and sets the damage value for the projectile
        /// </summary>
        public int Damage
        {
            get { return damage; }
            set { damage = value; }
        }

        /// <summary>
        /// Gets which entity shot the projectile
        /// </summary>
        public Entity ShotBy
        {
            get { return shotBy; }
        }

        #endregion

        #region Public methods


        #endregion
    }
}