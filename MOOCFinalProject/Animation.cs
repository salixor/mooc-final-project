﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOOCFinalProject
{
    /// <summary>
    /// An animation
    /// </summary>
    public class Animation
    {
        #region Fields

        // informations on the animation
        bool autoplays;
        bool loops;

        // fields used to play the animation
        bool playing;
        int elapsedFrameTime = 0;

        // frames used in the animation
        List<Frame> frames = new List<Frame>();
        Frame currentFrame;
        int currentFrameIndex = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new animation object
        /// </summary>
        /// <param name="autoplays">indicates if the animation automatically plays or not</param>
        /// <param name="loops">indicates if the animation loops or not</param>
        public Animation(bool autoplays, bool loops)
        {
            this.autoplays = autoplays;
            this.loops = loops;
            playing = autoplays;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the appropriate source rectangle for the animation
        /// </summary>
        public Rectangle SourceRectangle
        {
            get
            {
                // if there's no frame for this animation, something went wrong
                if (currentFrame is null || frames.Count == 0)
                    throw new AnimationException("Animation has no frame");

                return new Rectangle(
                    currentFrame.SpriteX,
                    currentFrame.SpriteY,
                    currentFrame.Width,
                    currentFrame.Height
                );
            }
        }

        /// <summary>
        /// Get the playing status of the animation
        /// </summary>
        public bool Playing
        {
            get { return playing; }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Updates the animation
        /// </summary>
        /// <param name="gameTime">the game time</param>
        public void Update(GameTime gameTime)
        {
            if (playing)
            {
                // check for advancing animation frame
                elapsedFrameTime += gameTime.ElapsedGameTime.Milliseconds;
                if (elapsedFrameTime > currentFrame.Duration)
                {
                    // reset frame timer
                    elapsedFrameTime = 0;

                    // advance the animation
                    if (currentFrameIndex < frames.Count - 1)
                    {
                        currentFrameIndex++;
                        currentFrame = frames[currentFrameIndex];
                    }
                    else
                        if (!loops) Stop(); else Reset();
                }
            }
        }

        /// <summary>
        /// Adds a frame to the animation
        /// </summary>
        /// <param name="width">the width of the frame</param>
        /// <param name="height">the height of the frame</param>
        /// <param name="x">the top left x position of the frame on the sprite</param>
        /// <param name="y">the top left y position of the frame on the sprite</param>
        /// <param name="duration">the duration of the frame in milliseconds</param>
        public Animation AddFrame(int width, int height, int x, int y, int duration)
        {
            // add the new frame to the list of frames
            Frame frame = new Frame(width, height, x, y, duration);
            frames.Add(frame);

            // update the current frame to the first one if needed
            currentFrame = (currentFrame is null) ? frames[0] : currentFrame;

            return this;
        }

        /// <summary>
        /// Adds a succession of frames with an uniform spacing (on one or multiple lines)
        /// </summary>
        /// <param name="frameCount">the number of frames to add</param>
        /// <param name="width">the width of each frame</param>
        /// <param name="height">the height of each frame</param>
        /// <param name="startX">the top left x position of the first frame on the sprite</param>
        /// <param name="startY">the top left y position of the first frame on the sprite</param>
        /// <param name="duration">the duration of each frame in milliseconds</param>
        /// <param name="breakLineEvery">the number of frames after which a line break happens on the sprite</param>
        public Animation AddFrames(int frameCount, int width, int height, int startX, int startY,
            int duration, int breakLineEvery = 0)
        {
            // check if a line break is really required, and calculate number of rows and cols
            bool lineBreaks = !(breakLineEvery == 0 || breakLineEvery == frameCount);
            int colCount = (lineBreaks) ? breakLineEvery : frameCount;
            int rowCount = (lineBreaks) ? (frameCount / breakLineEvery) + 1 : 1;

            for (int row = 0; row < rowCount; row++)
                for (int col = 0; col < colCount; col++)
                    if ((row + 1) * (col + 1) <= frameCount) // ensure we don't had empty frames
                        AddFrame(width, height, startX + width * col, startY + height * row, duration);

            return this;
        }

        /// <summary>
        /// Stops the animation
        /// </summary>
        public void Stop() { playing = false; }

        /// <summary>
        /// Plays the animation
        /// </summary>
        public void Play() { playing = true; }

        /// <summary>
        /// Restarts the animation
        /// </summary>
        public void Restart() { ResetTimer(); }

        /// <summary>
        /// Resets the animation
        /// </summary>
        public void Reset()
        {
            ResetTimer();
            playing = autoplays;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Resets the animation's timer
        /// </summary>
        private void ResetTimer()
        {
            currentFrame = (frames.Count > 0) ? frames[0] : null;
            elapsedFrameTime = 0;
            currentFrameIndex = 0;
        }

        #endregion
    }

    public class AnimationException : Exception
    {
        public AnimationException(string message) : base(message) { }
    }
}
