﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace MOOCFinalProject
{
    /// <summary>
    /// A character
    /// </summary>
    public class Character : Entity
    {
        #region Fields

        // mapping between possible character actions and keyboard keys
        Dictionary<CharacterAction, HashSet<Keys>> controls;

        // popups for character's damage and score updates
        List<TextDisplay> popups = new List<TextDisplay>();

        // current health and score for the character
        int health = GameConstants.CharacterStartHealth;
        int score = 0;

        // shooting support
        bool canShoot = true;
        int elapsedShootingCooldownMilliseconds = 0;
        Texture2D projectileSprite;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new character object
        /// </summary>
        /// <param name="contentManager">the content manager for loading content</param>
        /// <param name="spriteName">the sprite name for the character</param>
        /// <param name="x">the x location of the center of the character</param>
        /// <param name="y">the y location of the center of the character</param>
        /// <param name="width">the width of the character</param>
        /// <param name="height">the height of the character</param>
        /// <param name="velocity">the initial velocity of the character</param>
        /// <param name="projectileSprite">the sprite for the projectile</param>
        public Character(ContentManager contentManager, string spriteName,
            int x, int y, int width, int height, Vector2 velocity, Texture2D projectileSprite)
            : base(contentManager, spriteName, x, y, width, height, velocity, true)
        {
            controls = new Dictionary<CharacterAction, HashSet<Keys>>();
            this.projectileSprite = projectileSprite;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets and sets the health of the character
        /// </summary>
        public int Health
        {
            get { return health; }
            set { health = value; }
        }

        /// <summary>
        /// Gets and sets the score of the character
        /// </summary>
        public int Score
        {
            get { return score; }
            set { score = value; }
        }

        /// <summary>
        /// Gets current controls for this character
        /// </summary>
        public Dictionary<CharacterAction, HashSet<Keys>> Controls
        {
            get { return controls; }
        }

        /// <summary>
        /// Gets popups for this character
        /// </summary>
        public List<TextDisplay> PopUps
        {
            get { return popups; }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Updates the character
        /// </summary>
        /// <param name="gameTime">the game time</param>
        /// <param name="keyboard">the state of the keyboard</param>
        public void Update(GameTime gameTime, KeyboardState keyboard)
        {
            // update popup messages
            foreach (TextDisplay popup in popups)
                popup.Update(gameTime);

            // new velocity for character
            Vector2 newVelocity = Vector2.Zero;

            // change direction of entity according to keyboard (only 1 direction at a time)
            if (CheckAction(CharacterAction.MovingTop, keyboard))
                Direction = Direction.Top;
            if (CheckAction(CharacterAction.MovingBottom, keyboard))
                Direction = Direction.Bottom;
            if (CheckAction(CharacterAction.MovingLeft, keyboard))
                Direction = Direction.Left;
            if (CheckAction(CharacterAction.MovingRight, keyboard))
                Direction = Direction.Right;
            if (CheckAction(CharacterAction.MovingTop, keyboard) && CheckAction(CharacterAction.MovingLeft, keyboard))
                Direction = Direction.TopLeft;
            if (CheckAction(CharacterAction.MovingTop, keyboard) && CheckAction(CharacterAction.MovingRight, keyboard))
                Direction = Direction.TopRight;
            if (CheckAction(CharacterAction.MovingBottom, keyboard) && CheckAction(CharacterAction.MovingLeft, keyboard))
                Direction = Direction.BottomLeft;
            if (CheckAction(CharacterAction.MovingBottom, keyboard) && CheckAction(CharacterAction.MovingRight, keyboard))
                Direction = Direction.BottomRight;

            // move entity using keyboard (only 1 direction at a time)
            if (CheckAction(CharacterAction.MovingTop, keyboard))
                newVelocity.Y = -GameConstants.CharacterVelocity;
            if (CheckAction(CharacterAction.MovingBottom, keyboard))
                newVelocity.Y = GameConstants.CharacterVelocity;
            if (CheckAction(CharacterAction.MovingLeft, keyboard))
                newVelocity.X = -GameConstants.CharacterVelocity;
            if (CheckAction(CharacterAction.MovingRight, keyboard))
                newVelocity.X = GameConstants.CharacterVelocity;

            // if moving on a diagonal, fix issue with speed being too high
            newVelocity.X *= (newVelocity.X != 0 && newVelocity.Y != 0) ? (float)(1 / Math.Sqrt(2)) : 1;
            newVelocity.Y *= (newVelocity.X != 0 && newVelocity.Y != 0) ? (float)(1 / Math.Sqrt(2)) : 1;

            // update velocity
            Velocity = newVelocity;

            // change the state of the character accordingly to its movement
            //ChangeState(EntityState.Idle);
            if (newVelocity.X != 0 || newVelocity.Y != 0)
            {
                if (Direction == Direction.Top)
                    ChangeState(EntityState.MovingTop);
                if (Direction == Direction.Bottom)
                    ChangeState(EntityState.MovingBottom);
                if (Direction == Direction.Left)
                    ChangeState(EntityState.MovingLeft);
                if (Direction == Direction.Right)
                    ChangeState(EntityState.MovingRight);
                if (Direction == Direction.TopLeft)
                    ChangeState(EntityState.MovingTopLeft);
                if (Direction == Direction.TopRight)
                    ChangeState(EntityState.MovingTopRight);
                if (Direction == Direction.BottomLeft)
                    ChangeState(EntityState.MovingBottomLeft);
                if (Direction == Direction.BottomRight)
                    ChangeState(EntityState.MovingBottomRight);
            }

            // update shooting allowed
            if (!canShoot)
            {
                elapsedShootingCooldownMilliseconds += gameTime.ElapsedGameTime.Milliseconds;
                if (elapsedShootingCooldownMilliseconds >= GameConstants.CharacterShootingCooldown
                    || controls[CharacterAction.Shooting].All(key => keyboard.IsKeyUp(key)))
                {
                    canShoot = true;
                    elapsedShootingCooldownMilliseconds = 0;
                }
            }

            // make the character shoot a projectile, if appropriate key pressed and cooldown okay
            if (canShoot && controls[CharacterAction.Shooting].Any(key => keyboard.IsKeyDown(key)))
            {
                canShoot = false;

                // determine velocity and start position of projectile depending on aiming direction
                Vector2 projectile_velocity = Vector2.Zero;
                int projectileX = Location.X;
                int projectileY = Location.Y;

                if (Direction == Direction.Top || Direction == Direction.TopLeft || Direction == Direction.TopRight)
                {
                    projectile_velocity.Y = (float)-GameConstants.ProjectileVelocity;
                    projectileY -= DrawRectangle.Height / 2 + GameConstants.ProjectileHeight / 2;
                }
                if (Direction == Direction.Bottom || Direction == Direction.BottomLeft || Direction == Direction.BottomRight)
                {
                    projectile_velocity.Y = (float)GameConstants.ProjectileVelocity;
                    projectileY += DrawRectangle.Height / 2 + GameConstants.ProjectileHeight / 2;
                }
                if (Direction == Direction.Left || Direction == Direction.TopLeft || Direction == Direction.BottomLeft)
                {
                    projectile_velocity.X = (float)-GameConstants.ProjectileVelocity;
                    projectileX -= DrawRectangle.Width / 2 + GameConstants.ProjectileWidth / 2;
                }
                if (Direction == Direction.Right || Direction == Direction.TopRight || Direction == Direction.BottomRight)
                {
                    projectile_velocity.X = (float)GameConstants.ProjectileVelocity;
                    projectileX += DrawRectangle.Width / 2 + GameConstants.ProjectileWidth / 2;
                }

                // create projectile and add it to the list of projectiles
                Projectile p = new Projectile(
                    projectileSprite,
                    projectileX, projectileY,
                    GameConstants.ProjectileWidth, GameConstants.ProjectileHeight,
                    projectile_velocity,
                    this
                );
                Animation blinkingAnimation = new Animation(true, true).AddFrames(4,
                    GameConstants.ProjectileWidth, GameConstants.ProjectileHeight, 0, 0, 100);
                p.AddAnimation(EntityState.Idle, blinkingAnimation);
                Game1.AddProjectile(p);
            }

            // update position of popups for first player
            int countPopUps = 0;
            foreach (TextDisplay message in popups)
            {
                message.SetPosition(Location - new Point(0, 50 + countPopUps * 25));
                countPopUps++;
            }

            // clean out inactive pop ups
            for (int i = popups.Count - 1; i >= 0; i--)
                if (!popups[i].Displaying)
                    popups.RemoveAt(i);

            base.Update(gameTime);
        }

        /// <summary>
        /// Draws the character
        /// </summary>
        /// <param name="spriteBatch">the spritebatch</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            DrawPopUps(spriteBatch);
            base.Draw(spriteBatch);
        }

        /// <summary>
        /// Draws the popups
        /// </summary>
        /// <param name="spriteBatch">the spritebatch</param>
        public void DrawPopUps(SpriteBatch spriteBatch)
        {
            foreach (TextDisplay message in popups)
                message.Draw(spriteBatch);
        }

        /// <summary>
        /// Adds a binding to the character's control scheme
        /// </summary>
        /// <param name="action">the action the key will be bound to</param>
        /// <param name="key">the key to bind</param>
        public void AddControl(CharacterAction action, Keys key)
        {
            HashSet<Keys> existingKeys;
            if (controls.TryGetValue(action, out existingKeys))
            {
                existingKeys.Add(key);
                controls[action] = existingKeys;
            }
            else
            {
                HashSet<Keys> actionKeys = new HashSet<Keys>();
                actionKeys.Add(key);
                controls.Add(action, actionKeys);
            }
        }

        /// <summary>
        /// Removes a binding from the character's control scheme
        /// </summary>
        /// <param name="action">the action the key will be unbound from</param>
        /// <param name="key">the key to unbind</param>
        public void RemoveControl(CharacterAction action, Keys key)
        {
            HashSet<Keys> existingKeys;
            if (controls.TryGetValue(action, out existingKeys))
            {
                existingKeys.Remove(key);
                controls[action] = existingKeys;
            }
        }

        /// <summary>
        /// Checks for death of character
        /// </summary>
        public bool CheckDeath()
        {
            health = (health <= 0) ? 0 : health;
            return health <= 0;
        }

        /// <summary>
        /// Adds score to the character
        /// </summary>
        /// <param name="points">the amount of points to add</param>
        /// <param name="font">the font used for the popups</param>
        public void AddScore(int points, SpriteFont font)
        {
            score += points;
            AddScorePopup(points, font);
        }

        /// <summary>
        /// Removes health from the character
        /// </summary>
        /// <param name="damage">the amount of health to remove</param>
        /// <param name="font">the font used for the popups</param>
        public void RemoveHealth(int damage, SpriteFont font)
        {
            health -= damage;
            AddDamagePopup(damage, font);
        }

        /// <summary>
        /// Add a popup for a character
        /// </summary>
        /// <param name="str">the string to display in the popup</param>
        /// <param name="color">the color of the popup</param>
        /// <param name="font">the font used for the popups</param>
        public void AddPopup(String str, Color color, SpriteFont font)
        {
            foreach (TextDisplay message in popups)
                message.Position = message.Position - new Vector2(0, 25);

            TextDisplay popup = new TextDisplay(str, font, GameConstants.PopUpDuration, Location - new Point(0, 50));
            popup.Color = color;
            popups.Add(popup);
        }

        /// <summary>
        /// Add a score popup for a character
        /// </summary>
        /// <param name="score">the score to display in the popup</param>
        /// <param name="font">the font used for the popups</param>
        public void AddScorePopup(int score, SpriteFont font)
        {
            AddPopup("+" + score, GameConstants.GreenColor, font);
        }

        /// <summary>
        /// Add a damage popup for a character
        /// </summary>
        /// <param name="damage">the damage to display in the popup</param>
        /// <param name="font">the font used for the popups</param>
        public void AddDamagePopup(int damage, SpriteFont font)
        {
            AddPopup("-" + damage + "HP", GameConstants.RedColor, font);
        }

        /// <summary>
        /// Clears popups for a character
        /// </summary>
        public void ClearPopUps()
        {
            popups.Clear();
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Checks if an action is actually happening or not
        /// </summary>
        /// <param name="action">the action to check</param>
        /// <param name="keyboard">the state of the keyboard</param>
        private bool CheckAction(CharacterAction action, KeyboardState keyboard)
        {
            if (controls.ContainsKey(action))
                return controls[action].Any(key => keyboard.IsKeyDown(key));
            return false;
        }

        #endregion
    }
}
