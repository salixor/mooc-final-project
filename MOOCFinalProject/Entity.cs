﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MOOCFinalProject
{
    /// <summary>
    /// An entity
    /// </summary>
    public class Entity
    {
        #region Fields

        // object location and velocity
        Texture2D sprite;
        Rectangle drawRectangle;
        Vector2 velocity;

        // indicates which part of the sprite has to be drawn
        Rectangle sourceRectangle;

        // handles animation
        Dictionary<EntityState, Animation> animations = new Dictionary<EntityState, Animation>();
        Animation currentAnimation;

        // direction of the entity
        Direction direction = Direction.None;

        // state of the entity
        EntityState currentState = EntityState.Idle;
        bool active = true;
        bool keepOnScreen = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new entity object given the sprite's name
        /// </summary>
        /// <param name="contentManager">the content manager for loading content</param>
        /// <param name="spriteName">the sprite name for the entity</param>
        /// <param name="x">the x location of the center of the entity</param>
        /// <param name="y">the y location of the center of the entity</param>
        /// <param name="width">the width of the entity</param>
        /// <param name="height">the height of the entity</param>
        /// <param name="velocity">the initial velocity of the entity</param>
        /// <param name="keepOnScreen">indicates whether the entity shall stay on screen or not</param>
        public Entity(ContentManager contentManager, string spriteName,
            int x, int y, int width, int height, Vector2 velocity, bool keepOnScreen)
        {
            this.velocity = velocity;
            this.keepOnScreen = keepOnScreen;
            LoadContent(contentManager, spriteName, x, y, width, height);

            InitializeSourceRectangle(width, height);
        }

        /// <summary>
        /// Constructs a new entity object given the already loaded sprite
        /// </summary>
        /// <param name="sprite">the already loaded sprite for the entity</param>
        /// <param name="location">the center of the entity</param>
        public Entity(Texture2D sprite, Point center)
            : this(sprite, center.X, center.Y, sprite.Width, sprite.Height, Vector2.Zero, false) { }

        /// <summary>
        /// Constructs a new entity object given the already loaded sprite
        /// </summary>
        /// <param name="sprite">the already loaded sprite for the entity</param>
        /// <param name="location">the center of the entity</param>
        /// <param name="width">the width of the entity</param>
        /// <param name="height">the height of the entity</param>
        public Entity(Texture2D sprite, Point center, int width, int height)
            : this(sprite, center.X, center.Y, width, height, Vector2.Zero, false) { }

        /// <summary>
        /// Constructs a new entity object given the already loaded sprite
        /// </summary>
        /// <param name="sprite">the already loaded sprite for the entity</param>
        /// <param name="x">the x location of the center of the entity</param>
        /// <param name="y">the y location of the center of the entity</param>
        public Entity(Texture2D sprite, int x, int y)
            : this(sprite, x, y, sprite.Width, sprite.Height, Vector2.Zero, false) { }

        /// <summary>
        /// Constructs a new entity object given the already loaded sprite
        /// </summary>
        /// <param name="sprite">the already loaded sprite for the entity</param>
        /// <param name="x">the x location of the center of the entity</param>
        /// <param name="y">the y location of the center of the entity</param>
        /// <param name="velocity">the initial velocity of the entity</param>
        /// <param name="keepOnScreen">indicates whether the entity shall stay on screen or not</param>
        public Entity(Texture2D sprite, int x, int y, Vector2 velocity, bool keepOnScreen)
            : this(sprite, x, y, sprite.Width, sprite.Height, velocity, keepOnScreen) { }

        /// <summary>
        /// Constructs a new entity object given the already loaded sprite
        /// </summary>
        /// <param name="sprite">the already loaded sprite for the entity</param>
        /// <param name="x">the x location of the center of the entity</param>
        /// <param name="y">the y location of the center of the entity</param>
        /// <param name="width">the width of the entity</param>
        /// <param name="height">the height of the entity</param>
        /// <param name="velocity">the initial velocity of the entity</param>
        /// <param name="keepOnScreen">indicates whether the entity shall stay on screen or not</param>
        public Entity(Texture2D sprite, int x, int y,
            int width, int height, Vector2 velocity, bool keepOnScreen)
        {
            this.sprite = sprite;
            this.velocity = velocity;
            this.keepOnScreen = keepOnScreen;
            this.drawRectangle = new Rectangle(x - width / 2, y - height / 2, width, height);

            InitializeSourceRectangle(width, height);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the width of the entity
        /// </summary>
        public int Width
        {
            get { return drawRectangle.Width; }
        }

        /// <summary>
        /// Gets the height of the entity
        /// </summary>
        public int Height
        {
            get { return drawRectangle.Height; }
        }

        /// <summary>
        /// Gets the sprite of the entity
        /// </summary>
        public Texture2D Sprite
        {
            get { return sprite; }
        }

        /// <summary>
        /// Gets the collision rectangle for the entity
        /// </summary>
        public Rectangle CollisionRectangle
        {
            get { return drawRectangle; }
        }

        /// <summary>
        /// Gets and sets the draw rectangle for the entity
        /// </summary>
        public Rectangle DrawRectangle
        {
            get { return drawRectangle; }
            set { drawRectangle = value; }
        }

        /// <summary>
        /// Gets and sets the source rectangle for the entity
        /// </summary>
        public Rectangle SourceRectangle
        {
            get { return sourceRectangle; }
            set { sourceRectangle = value; }
        }

        /// <summary>
        /// Gets and sets the direction of the entity
        /// </summary>
        public Direction Direction
        {
            get { return direction; }
            set { direction = value; }
        }

        /// <summary>
        /// Gets the location of the entity
        /// </summary>
        public Point Location
        {
            get { return drawRectangle.Center; }
        }

        /// <summary>
        /// Gets the position of the entity
        /// </summary>
        public Point Position
        {
            get { return drawRectangle.Location; }
        }

        /// <summary>
        /// Gets and sets the velocity of the entity
        /// </summary>
        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        /// <summary>
        /// Gets and sets whether or not the entity is active
        /// </summary>
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        /// <summary>
        /// Gets animations for entity
        /// </summary>
        public Dictionary<EntityState, Animation> Animations
        {
            get { return animations; }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Updates the entity
        /// </summary>
        /// <param name="gameTime">the game time</param>
        public virtual void Update(GameTime gameTime)
        {
            // move entity depending on velocity and time
            drawRectangle.X += (int)(velocity.X * gameTime.ElapsedGameTime.Milliseconds);
            drawRectangle.Y += (int)(velocity.Y * gameTime.ElapsedGameTime.Milliseconds);

            // clamp entity in window, if the entity has to stay on the screen
            if (keepOnScreen)
            {
                if (drawRectangle.X < 0)
                    drawRectangle.X = 0;
                if (drawRectangle.X > GameConstants.WindowWidth - Width)
                    drawRectangle.X = GameConstants.WindowWidth - Width;
                if (drawRectangle.Y < 0)
                    drawRectangle.Y = 0;
                if (drawRectangle.Y > GameConstants.WindowHeight - Height)
                    drawRectangle.Y = GameConstants.WindowHeight - Height;
            }

            // mark the entity as inactive if it's out of screen (and should not stay on screen)
            if (!keepOnScreen)
                active = (drawRectangle.X >= 0 - Width
                   && drawRectangle.X <= GameConstants.WindowWidth + Width
                   && drawRectangle.Y >= 0 - Height
                   && drawRectangle.Y <= GameConstants.WindowHeight + Height);
            else
                active = true;
            
            // retrieve the animation associated with the state of the entity (if there are animations)
            animations.TryGetValue(currentState, out currentAnimation);
            
            // play the current animation if there's one
            if (!(currentAnimation is null)) {
                currentAnimation.Update(gameTime);
                sourceRectangle = currentAnimation.SourceRectangle;
            }
            // if there's no current animation, draw the sprite at its starting point
            else
                sourceRectangle = new Rectangle(0, 0, drawRectangle.Width, drawRectangle.Height);
        }

        /// <summary>
        /// Draws the entity
        /// </summary>
        /// <param name="spriteBatch">the spritebatch</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, drawRectangle, sourceRectangle, Color.White);
        }

        /// <summary>
        /// Adds an animation to the entity
        /// </summary>
        /// <param name="state">the entity state in which this animation should play</param>
        /// <param name="animation">the animation</param>
        public void AddAnimation(EntityState state, Animation animation)
        {
            if (!(animations.ContainsKey(state)))
                animations.Add(state, animation);
        }

        /// <summary>
        /// Changes the state of the entity
        /// </summary>
        /// <param name="state">the new entity state</param>
        public void ChangeState(EntityState state)
        {
            if (state == currentState)
                return;
            currentState = state;

            // reset the current animation
            if (!(currentAnimation is null))
                currentAnimation.Reset();

            // pick the right one for the new state (if there's one)
            animations.TryGetValue(currentState, out currentAnimation);
        }

        /// <summary>
        /// Checks if the entity collides another entity
        /// </summary>
        /// <param name="other">the entity to check a collision with</param>
        /// <param name="pixelPerfect">specifies whether or not collision should be pixel perfect</param>
        public bool CollidesWith(Entity other, bool pixelPerfect = true)
        {
            bool boundsCollide = CollisionRectangle.Intersects(other.CollisionRectangle);

            if (pixelPerfect && boundsCollide)
            {
                return CollisionUtils.PerPixelCollision(this, other);
            }

            return CollisionRectangle.Intersects(other.CollisionRectangle);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Initialize the rectangle used to draw a specific part of the sprite
        /// </summary>
        /// <param name="width">the width of the entity</param>
        /// <param name="height">the height of the entity</param>
        private void InitializeSourceRectangle(int width, int height)
        {
            if (!(currentAnimation is null))
                sourceRectangle = currentAnimation.SourceRectangle;
            else
                sourceRectangle = new Rectangle(0, 0, width, height);
        }

        /// <summary>
        /// Loads the content for the entity
        /// </summary>
        /// <param name="contentManager">the content manager to use</param>
        /// <param name="spriteName">the name of the sprite for the entity</param>
        /// <param name="x">the x location of the center of the entity</param>
        /// <param name="y">the y location of the center of the entity</param>
        /// <param name="width">the width of the entity</param>
        /// <param name="height">the height of the entity</param>
        private void LoadContent(ContentManager contentManager, string spriteName,
            int x, int y, int width, int height)
        {
            sprite = contentManager.Load<Texture2D>(spriteName);
            drawRectangle = new Rectangle(x - width / 2, y - height / 2, width, height);
        }

        #endregion
    }
}