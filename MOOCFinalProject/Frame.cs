﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOOCFinalProject
{
    /// <summary>
    /// A frame of animation
    /// </summary>
    public class Frame
    {
        #region Fields

        // dimensions of the frame
        int frameWidth;
        int frameHeight;

        // position of the frame on the appropriate sprite
        int spriteTopLeftX;
        int spriteTopLeftY;

        // duration of the frame
        int frameDurationMilliseconds;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new animation object
        /// </summary>
        /// <param name="width">the width of the frame</param>
        /// <param name="height">the height of the frame</param>
        /// <param name="x">the top left x position of the frame on the sprite</param>
        /// <param name="y">the top left y position of the frame on the sprite</param>
        /// <param name="durationMilliseconds">the duration of the frame in milliseconds</param>
        public Frame(int width, int height, int x, int y, int durationMilliseconds)
        {
            frameWidth = width;
            frameHeight = height;
            spriteTopLeftX = x;
            spriteTopLeftY = y;
            frameDurationMilliseconds = durationMilliseconds;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the width of the frame
        /// </summary>
        public int Width
        {
            get { return frameWidth; }
        }

        /// <summary>
        /// Gets the height of the frame
        /// </summary>
        public int Height
        {
            get { return frameHeight; }
        }

        /// <summary>
        /// Gets the top left x position of the frame
        /// </summary>
        public int SpriteX
        {
            get { return spriteTopLeftX; }
        }

        /// <summary>
        /// Gets the top left y position of the frame
        /// </summary>
        public int SpriteY
        {
            get { return spriteTopLeftY; }
        }

        /// <summary>
        /// Gets the duration of the frame
        /// </summary>
        public int Duration
        {
            get { return frameDurationMilliseconds; }
        }

        #endregion
    }
}
