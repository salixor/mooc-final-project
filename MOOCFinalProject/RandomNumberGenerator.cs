﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MOOCFinalProject
{
    /// <summary>
    /// Provides a single generator for random numbers throughout the game
    /// </summary>
    public static class RandomNumberGenerator
    {
        #region Fields

        static Random rand;

        #endregion

        #region Public methods

        /// <summary>
        /// Initializes the random number generator
        /// </summary>
        public static void Initialize()
        {
            rand = new Random();
        }

        /// <summary>
        /// Returns a random number higher than minValue and less than maxValue (exclusive)
        /// </summary>
        /// <param name="minValue">the inclusive min value</param>
        /// <param name="maxValue">the exclusive max value</param>
        /// <returns>the random number</returns>
        public static int RandomInt(int minValue, int maxValue)
        {
            return minValue + rand.Next(maxValue);
        }

        /// <summary>
        /// Returns a random number higher than minValue and less than maxValue (exclusive)
        /// </summary>
        /// <param name="minValue">the inclusive min value</param>
        /// <param name="maxValue">the exclusive max value</param>
        /// <returns>the random number</returns>
        public static float RandomFloat(int minValue, int maxValue)
        {
            return RandomFloat((float)minValue, (float)maxValue);
        }

        /// <summary>
        /// Returns a random number higher than minValue and less than maxValue (exclusive)
        /// </summary>
        /// <param name="minValue">the inclusive min value</param>
        /// <param name="maxValue">the exclusive max value</param>
        /// <returns>the random number</returns>
        public static float RandomFloat(double minValue, double maxValue)
        {
            return RandomFloat((float)minValue, (float)maxValue);
        }

        /// <summary>
        /// Returns a random number higher than minValue and less than maxValue (exclusive)
        /// </summary>
        /// <param name="minValue">the inclusive min value</param>
        /// <param name="maxValue">the exclusive max value</param>
        /// <returns>the random number</returns>
        public static float RandomFloat(float minValue, float maxValue)
        {
            return (float)(minValue + rand.NextDouble() * maxValue);
        }

        /// <summary>
        /// Returns a random number between 0.0 and 1.0
        /// </summary>
        /// <returns>the random number</returns>
        public static double NextDouble()
        {
            return rand.NextDouble();
        }

        #endregion
    }
}
