﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOOCFinalProject
{
    /// <summary>
    /// A text message
    /// </summary>
    public class TextDisplay
    {
        #region Fields

        // text message properties
        string text;
        Color color = Color.Black;
        TextDisplayAlignment alignment;

        // variables used to properly display the message
        SpriteFont font;
        Vector2 wantedPosition;
        Vector2 position;

        // display duration, if set to expire
        bool displaying = true;
        int elaspedDisplayTime = 0;
        int duration = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="font">the sprite font for the message</param>
        /// <param name="wantedPosition">the wanted position of the message</param>
        /// <param name="alignment">the alignment of the message</param>
        public TextDisplay(SpriteFont font, Vector2 wantedPosition,
            TextDisplayAlignment alignment = TextDisplayAlignment.Center)
            : this("", font, 0, wantedPosition, alignment) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="font">the sprite font for the message</param>
        /// <param name="wantedPosition">the wanted position of the message</param>
        /// <param name="color">the color of the messages</param>
        /// <param name="alignment">the alignment of the message</param>
        public TextDisplay(SpriteFont font, Vector2 wantedPosition,
            Color color, TextDisplayAlignment alignment = TextDisplayAlignment.Center)
            : this("", font, 0, wantedPosition, color, alignment) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">the text for the message</param>
        /// <param name="font">the sprite font for the message</param>
        /// <param name="wantedPosition">the wanted position of the message</param>
        /// <param name="color">the color of the messages</param>
        /// <param name="alignment">the alignment of the message</param>
        public TextDisplay(string text, SpriteFont font, Vector2 wantedPosition,
            Color color, TextDisplayAlignment alignment = TextDisplayAlignment.Center)
            : this(text, font, 0, wantedPosition, color, alignment) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">the text for the message</param>
        /// <param name="font">the sprite font for the message</param>
        /// <param name="wantedPosition">the wanted position of the message</param>
        /// <param name="alignment">the alignment of the message</param>
        public TextDisplay(string text, SpriteFont font, Vector2 wantedPosition,
            TextDisplayAlignment alignment = TextDisplayAlignment.Center)
            : this(text, font, 0, wantedPosition, alignment) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">the text for the message</param>
        /// <param name="font">the sprite font for the message</param>
        /// <param name="wantedPosition">the wanted position of the message</param>
        /// <param name="alignment">the alignment of the message</param>
        public TextDisplay(string text, SpriteFont font, Point wantedPosition,
            TextDisplayAlignment alignment = TextDisplayAlignment.Center)
            : this(text, font, 0, new Vector2(wantedPosition.X, wantedPosition.Y), alignment) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">the text for the message</param>
        /// <param name="font">the sprite font for the message</param>
        /// <param name="duration">the wanted duration of the message</param>
        /// <param name="wantedPosition">the wanted position of the message</param>
        /// <param name="alignment">the alignment of the message</param>
        public TextDisplay(string text, SpriteFont font, int duration, Point wantedPosition,
            TextDisplayAlignment alignment = TextDisplayAlignment.Center)
            : this(text, font, duration, new Vector2(wantedPosition.X, wantedPosition.Y), alignment) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">the text for the message</param>
        /// <param name="font">the sprite font for the message</param>
        /// <param name="duration">the wanted duration of the message</param>
        /// <param name="wantedPosition">the wanted position of the message</param>
        /// <param name="alignment">the alignment of the message</param>
        public TextDisplay(string text, SpriteFont font, int duration, Vector2 wantedPosition,
            TextDisplayAlignment alignment = TextDisplayAlignment.Center)
            : this(text, font, duration, wantedPosition, Color.Black, alignment) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">the text for the message</param>
        /// <param name="font">the sprite font for the message</param>
        /// <param name="duration">the wanted duration of the message</param>
        /// <param name="wantedPosition">the wanted position of the message</param>
        /// <param name="color">the color of the messages</param>
        /// <param name="alignment">the alignment of the message</param>
        public TextDisplay(string text, SpriteFont font, int duration, Vector2 wantedPosition,
            Color color, TextDisplayAlignment alignment = TextDisplayAlignment.Center)
        {
            this.text = text;
            this.font = font;
            this.duration = duration;
            this.wantedPosition = wantedPosition;
            this.color = color;
            this.alignment = alignment;

            UpdatePosition();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Sets the text for the message
        /// </summary>
        public string Text
        {
            set
            {
                text = value;
                UpdatePosition();
            }
        }

        /// <summary>
        /// Sets the color for the message
        /// </summary>
        public Color Color
        {
            set { color = value; }
        }

        /// <summary>
        /// Gets and sets whether or not the text is displayed
        /// </summary>
        public bool Displaying
        {
            get { return displaying; }
            set { displaying = value; }
        }

        /// <summary>
        /// Gets and sets the position of the message
        /// </summary>
        public Vector2 Position
        {
            get { return wantedPosition; }
            set
            {
                wantedPosition = value;
                UpdatePosition();
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Updates the position at which text has to be drawn
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void UpdatePosition()
        {
            // determine sixe of text
            float textWidth = font.MeasureString(text).X;
            float textHeight = font.MeasureString(text).Y;

            // determine the appropriate position according to the specified alignment
            if (alignment == TextDisplayAlignment.Left)
                position = new Vector2(wantedPosition.X, 0);
            else if (alignment == TextDisplayAlignment.Center)
                position = new Vector2(wantedPosition.X - textWidth / 2, 0);
            else
                position = new Vector2(wantedPosition.X - textWidth, 0);
            position.Y = wantedPosition.Y - textHeight / 2;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Updates the message
        /// </summary>
        /// <param name="gameTime">the game time</param>
        public void Update(GameTime gameTime)
        {
            if (displaying && duration != 0)
            {
                // check for timer
                elaspedDisplayTime += gameTime.ElapsedGameTime.Milliseconds;
                if (elaspedDisplayTime > duration)
                    displaying = false;
            }
        }

        /// <summary>
        /// Draws the message
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            if (displaying)
                spriteBatch.DrawString(font, text, position, color);
        }

        /// <summary>
        /// Moves the text display
        /// </summary>
        /// <param name="position"></param>
        public void SetPosition(Point position)
        {
            Position = new Vector2(position.X, position.Y);
        }

        #endregion
    }

    /// <summary>
    /// An enumeration for possible alignments for text messages
    /// </summary>
    public enum TextDisplayAlignment
    {
        Left,
        Center,
        Right
    }
}
