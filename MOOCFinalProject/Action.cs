﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOOCFinalProject
{
    /// <summary>
    /// An enumeration for possible actions
    /// </summary>
    public enum Action
    {
    }

    /// <summary>
    /// An enumeration for possible character actions
    /// </summary>
    public enum CharacterAction
    {
        MovingTop,
        MovingBottom,
        MovingLeft,
        MovingRight,
        Shooting
    }
}
