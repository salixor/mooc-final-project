﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MOOCFinalProject
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        #region Fields

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // game state
        static GameState currentState = GameState.Starting;

        // menu buttons
        static List<MenuButton> menuButtons = new List<MenuButton>();
        static MenuButton menuButton;
        static MenuButton startSoloButton;
        static MenuButton startVersusButton;
        static MenuButton continueSoloButton;
        static MenuButton continueVersusButton;
        static MenuButton resumeSoloButton;
        static MenuButton resumeVersusButton;
        static MenuButton quitButton;

        // backgrounds
        static Texture2D gameBackground;
        static Texture2D menuBackground;

        // menu and pause texts
        TextDisplay menuText;
        TextDisplay pauseText;
        TextDisplay finishText;

        // character related variables
        Character character1;
        Character character2;
        List<Character> characters = new List<Character>();
        int character1TotalScore = 0;
        int character2TotalScore = 0;

        // projectiles related variables
        static Texture2D projectileSprite1;
        static Texture2D projectileSprite2;
        static List<Projectile> projectiles = new List<Projectile>();

        // asteroid related variables
        static Texture2D asteroidSprite;
        static List<Asteroid> asteroids = new List<Asteroid>();

        // explosion related variables
        static Texture2D explosionSprite;
        static List<Entity> explosions = new List<Entity>();

        // text display support
        SpriteFont font;
        TextDisplay character1HealthMessage;
        TextDisplay character2HealthMessage;
        TextDisplay character1ScoreMessage;
        TextDisplay character2ScoreMessage;
        TextDisplay character1TotalScoreMessage;
        TextDisplay character2TotalScoreMessage;
        List<TextDisplay> messages = new List<TextDisplay>();

        // keep track of escape key
        bool escapeKeyPressed = false;

        // total time of currently running game (solo or versus)
        int currentGameElapsedTime = 0;

        #endregion

        #region Constructors

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // set resolution
            graphics.PreferredBackBufferWidth = GameConstants.WindowWidth;
            graphics.PreferredBackBufferHeight = GameConstants.WindowHeight;

            // make mouse visible
            IsMouseVisible = true;
        }

        #endregion

        #region Properties

        #endregion

        #region Protected methods

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            RandomNumberGenerator.Initialize();
            InitializeCharacters();
            base.Initialize();
        }

        /// <summary>
        /// Initialize characters
        /// </summary>
        /// <param name="characterCount">the number of characters</param>
        protected void InitializeCharacters()
        {
            // load first character
            character1 = new Character(Content, @"graphics\ship1",
                (int)GameConstants.Character1StartLocation.X, (int)GameConstants.Character1StartLocation.Y,
                64, 64, Vector2.Zero, projectileSprite1);
            characters.Add(character1);

            character1.Direction = Direction.Right;
            character1.ChangeState(EntityState.MovingRight);

            character1.AddControl(CharacterAction.MovingTop, Keys.Z);
            character1.AddControl(CharacterAction.MovingBottom, Keys.S);
            character1.AddControl(CharacterAction.MovingLeft, Keys.Q);
            character1.AddControl(CharacterAction.MovingRight, Keys.D);
            character1.AddControl(CharacterAction.Shooting, Keys.Space);

            // load second character
            character2 = new Character(Content, @"graphics\ship2",
                (int)GameConstants.Character2StartLocation.X, (int)GameConstants.Character2StartLocation.Y,
                64, 64, Vector2.Zero, projectileSprite2);
            characters.Add(character2);

            character2.Direction = Direction.Left;
            character2.ChangeState(EntityState.MovingLeft);

            character2.AddControl(CharacterAction.MovingTop, Keys.NumPad8);
            character2.AddControl(CharacterAction.MovingBottom, Keys.NumPad5);
            character2.AddControl(CharacterAction.MovingLeft, Keys.NumPad4);
            character2.AddControl(CharacterAction.MovingRight, Keys.NumPad6);
            character2.AddControl(CharacterAction.Shooting, Keys.Enter);

            // add animations
            Animation movingTopAnimation = new Animation(true, true).AddFrames(4, 64, 64, 0, 0, 100);
            Animation movingLeftAnimation = new Animation(true, true).AddFrames(4, 64, 64, 0, 192, 100);
            Animation movingBottomAnimation = new Animation(true, true).AddFrames(4, 64, 64, 0, 128, 100);
            Animation movingRightAnimation = new Animation(true, true).AddFrames(4, 64, 64, 0, 64, 100);
            Animation movingTopLeftAnimation = new Animation(true, true).AddFrames(4, 64, 64, 0, 448, 100);
            Animation movingTopRightAnimation = new Animation(true, true).AddFrames(4, 64, 64, 0, 256, 100);
            Animation movingBottomLeftAnimation = new Animation(true, true).AddFrames(4, 64, 64, 0, 384, 100);
            Animation movingBottomRightAnimation = new Animation(true, true).AddFrames(4, 64, 64, 0, 320, 100);

            foreach (Character character in characters)
            {
                character.AddAnimation(EntityState.MovingTop, movingTopAnimation);
                character.AddAnimation(EntityState.MovingLeft, movingLeftAnimation);
                character.AddAnimation(EntityState.MovingBottom, movingBottomAnimation);
                character.AddAnimation(EntityState.MovingRight, movingRightAnimation);
                character.AddAnimation(EntityState.MovingTopLeft, movingTopLeftAnimation);
                character.AddAnimation(EntityState.MovingTopRight, movingTopRightAnimation);
                character.AddAnimation(EntityState.MovingBottomLeft, movingBottomLeftAnimation);
                character.AddAnimation(EntityState.MovingBottomRight, movingBottomRightAnimation);
            }
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // load sprite for projectiles
            projectileSprite1 = Content.Load<Texture2D>(@"graphics\projectile1");
            projectileSprite2 = Content.Load<Texture2D>(@"graphics\projectile2");

            // load sprite for asteroid
            asteroidSprite = Content.Load<Texture2D>(@"graphics\asteroid");

            // load sprite for explosion
            explosionSprite = Content.Load<Texture2D>(@"graphics\explosion");

            // load sprite font
            font = Content.Load<SpriteFont>(@"fonts\Arial20");

            // backgrounds
            gameBackground = Content.Load<Texture2D>(@"graphics\background");
            menuBackground = new Texture2D(GraphicsDevice, 1, 1);
            menuBackground.SetData<Color>(new Color[] { Color.Black });

            // initialize menu and pause texts
            menuText = new TextDisplay(GameConstants.MenuText, font, GameConstants.MenuTextLocation, Color.White);
            pauseText = new TextDisplay(GameConstants.PauseText, font, GameConstants.PauseTextLocation, Color.White);
            finishText = new TextDisplay(GameConstants.FinishText, font, GameConstants.FinishTextLocation, Color.White);

            // initialize character's health related messages
            character1HealthMessage = new TextDisplay(font,
                GameConstants.Player1HealthLocation, Color.White, TextDisplayAlignment.Left);
            character2HealthMessage = new TextDisplay(font,
                GameConstants.Player2HealthLocation, Color.White, TextDisplayAlignment.Right);
            messages.Add(character1HealthMessage);
            messages.Add(character2HealthMessage);

            // initialize character's score related messages
            character1ScoreMessage = new TextDisplay(font,
                GameConstants.Player1ScoreLocation, Color.White, TextDisplayAlignment.Left);
            character2ScoreMessage = new TextDisplay(font,
                GameConstants.Player2ScoreLocation, Color.White, TextDisplayAlignment.Right);
            messages.Add(character1ScoreMessage);
            messages.Add(character2ScoreMessage);

            // initialize character's total score related messages
            character1TotalScoreMessage = new TextDisplay(font,
                GameConstants.Player1TotalScoreLocation, Color.White, TextDisplayAlignment.Left);
            character2TotalScoreMessage = new TextDisplay(font,
                GameConstants.Player2TotalScoreLocation, Color.White, TextDisplayAlignment.Right);
            character1TotalScoreMessage.Text = GameConstants.TotalScorePrefix + character1TotalScore.ToString();
            character2TotalScoreMessage.Text = GameConstants.TotalScorePrefix + character2TotalScore.ToString();
            messages.Add(character1TotalScoreMessage);
            messages.Add(character2TotalScoreMessage);

            // create menu button
            menuButton = new MenuButton(
                Content.Load<Texture2D>(@"graphics\menu"),
                GameConstants.WindowWidth / 2, GameConstants.MenuButtonOffset,
                GameConstants.ButtonWidth, GameConstants.ButtonHeight,
                GameState.Menu
            );

            // create start solo button
            startSoloButton = new MenuButton(
                Content.Load<Texture2D>(@"graphics\startSolo"),
                GameConstants.WindowWidth / 2, GameConstants.StartSoloButtonOffset,
                GameConstants.ButtonWidth, GameConstants.ButtonHeight,
                GameState.PlayingSolo
            );

            // create start versus button
            startVersusButton = new MenuButton(
                Content.Load<Texture2D>(@"graphics\startVersus"),
                GameConstants.WindowWidth / 2, GameConstants.StartVersusButtonOffset,
                GameConstants.ButtonWidth, GameConstants.ButtonHeight,
                GameState.PlayingVersus
            );

            // create continue solo button
            continueSoloButton = new MenuButton(
                Content.Load<Texture2D>(@"graphics\replay"),
                GameConstants.WindowWidth / 2, GameConstants.ContinueButtonOffset,
                GameConstants.ButtonWidth, GameConstants.ButtonHeight,
                GameState.ResetSolo
            );

            // create continue versus button
            continueVersusButton = new MenuButton(
                Content.Load<Texture2D>(@"graphics\continue"),
                GameConstants.WindowWidth / 2, GameConstants.ContinueButtonOffset,
                GameConstants.ButtonWidth, GameConstants.ButtonHeight,
                GameState.ResetVersus
            );

            // create resume solo button
            resumeSoloButton = new MenuButton(
                Content.Load<Texture2D>(@"graphics\resume"),
                GameConstants.WindowWidth / 2, GameConstants.ContinueButtonOffset,
                GameConstants.ButtonWidth, GameConstants.ButtonHeight,
                GameState.PlayingSolo
            );

            // create resume versus button
            resumeVersusButton = new MenuButton(
                Content.Load<Texture2D>(@"graphics\resume"),
                GameConstants.WindowWidth / 2, GameConstants.ContinueButtonOffset,
                GameConstants.ButtonWidth, GameConstants.ButtonHeight,
                GameState.PlayingVersus
            );

            // create quit button
            quitButton = new MenuButton(
                Content.Load<Texture2D>(@"graphics\quit"),
                GameConstants.WindowWidth / 2, GameConstants.QuitButtonOffset,
                GameConstants.ButtonWidth, GameConstants.ButtonHeight,
                GameState.Exit
            );
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // exit game if current state is exit
            if (currentState == GameState.Exit)
                Exit();

            // handle escape key press
            if (!escapeKeyPressed && Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                escapeKeyPressed = true;

                // exit if escape pressed in menu
                if (currentState == GameState.Menu)
                    Exit();

                // go to pause menu if playing
                else if (currentState == GameState.PlayingSolo)
                    ChangeState(GameState.PauseSolo);
                else if (currentState == GameState.PlayingVersus)
                    ChangeState(GameState.PauseVersus);

                // resume game if paused
                else if (currentState == GameState.PauseSolo)
                    ChangeState(GameState.PlayingSolo);
                else if (currentState == GameState.PauseVersus)
                    ChangeState(GameState.PlayingVersus);

                // go back to menu if game ended
                else if (currentState == GameState.FinishedSolo || currentState == GameState.FinishedVersus)
                    ChangeState(GameState.Menu);
            }

            // reset escape key press when key is released
            if (Keyboard.GetState().IsKeyUp(Keys.Escape))
                escapeKeyPressed = false;

            // get current mouse and keyboard states
            MouseState mouse = Mouse.GetState();
            KeyboardState keyboard = Keyboard.GetState();

            // update buttons
            UpdateDisplayedButtons();
            foreach (MenuButton button in menuButtons)
                button.Update(mouse);

            // update messages
            UpdateDisplayedMessages();

            // game state-specific processing
            switch (currentState)
            {
                // game is loading
                case GameState.Starting:
                    ChangeState(GameState.Menu);
                    break;

                // game is on menu screen loading
                case GameState.Menu:
                    currentGameElapsedTime = 0;

                    InitializeCharacters();
                    character1TotalScore = 0;
                    character2TotalScore = 0;
                    character1TotalScoreMessage.Text = GameConstants.TotalScorePrefix + character1TotalScore.ToString();
                    character2TotalScoreMessage.Text = GameConstants.TotalScorePrefix + character2TotalScore.ToString();

                    break;

                // game in versus mode is reset
                case GameState.ResetSolo:
                    currentGameElapsedTime = 0;
                    InitializeCharacters();
                    ChangeState(GameState.PlayingSolo);

                    break;

                // game in versus mode is reset
                case GameState.ResetVersus:
                    currentGameElapsedTime = 0;
                    InitializeCharacters();
                    ChangeState(GameState.PlayingVersus);

                    break;

                // game is playing in solo or versus mode
                case GameState.PlayingSolo:
                case GameState.PlayingVersus:
                    // update current elapsed game time
                    currentGameElapsedTime += gameTime.ElapsedGameTime.Milliseconds;

                    // update projectile objects
                    foreach (Projectile projectile in projectiles)
                        projectile.Update(gameTime);

                    // update asteroid objects
                    foreach (Asteroid asteroid in asteroids)
                        asteroid.Update(gameTime);

                    // update explosion objects
                    foreach (Entity explosion in explosions)
                        explosion.Update(gameTime);

                    // update current game if no character is dead
                    if (!character1.CheckDeath() && !character2.CheckDeath())
                    {
                        // update character objects
                        character1.Update(gameTime, keyboard);
                        if (currentState == GameState.PlayingVersus) character2.Update(gameTime, keyboard);

                        // check and resolve collisions between an asteroid and projectiles
                        foreach (Projectile projectile in projectiles)
                        {
                            if (!(projectile.Active))
                                continue;

                            if (currentState == GameState.PlayingVersus)
                            {
                                CheckCollisionCharacterAndProjectile(character1, character2, projectile);
                                CheckCollisionCharacterAndProjectile(character2, character1, projectile);
                            }

                            foreach (Asteroid asteroid in asteroids)
                            {
                                if (asteroid.Active && projectile.CollidesWith(asteroid))
                                {
                                    AddExplosion(asteroid.Location);
                                    ((Character)projectile.ShotBy).AddScore(GameConstants.ScorePerAsteroid, font);
                                    asteroid.Active = false;
                                    projectile.Active = false;
                                }
                            }
                        }

                        // check and resolve collisions between a character and asteroids
                        foreach (Asteroid asteroid in asteroids)
                        {
                            // asteroid collides with first player
                            if (asteroid.Active && asteroid.CollidesWith(character1))
                            {
                                AddExplosion(asteroid.Location);
                                character1.RemoveHealth(asteroid.Damage, font);
                                asteroid.Active = false;
                            }

                            // asteroid collides with second player (versus mode only)
                            if (asteroid.Active && asteroid.CollidesWith(character2) && currentState == GameState.PlayingVersus)
                            {
                                AddExplosion(asteroid.Location);
                                character2.RemoveHealth(asteroid.Damage, font);
                                asteroid.Active = false;
                            }
                        }
                    }
                    else
                    {
                        // if one of the characters is dead, all objects are inactive
                        projectiles.Clear();
                        asteroids.Clear();
                        explosions.Clear();

                        // give points to the total score of the appropriate character (versus mode only)
                        if (currentState == GameState.PlayingVersus)
                        {
                            if (character1.CheckDeath())
                                character2TotalScore += character2.Score;
                            if (character2.CheckDeath())
                                character1TotalScore += character1.Score;
                            character1TotalScoreMessage.Text = GameConstants.TotalScorePrefix + character1TotalScore.ToString();
                            character2TotalScoreMessage.Text = GameConstants.TotalScorePrefix + character2TotalScore.ToString();
                        }

                        // go to the end game screen
                        if (currentState == GameState.PlayingSolo)
                            ChangeState(GameState.FinishedSolo);
                        if (currentState == GameState.PlayingVersus)
                            ChangeState(GameState.FinishedVersus);
                    }

                    // clean out inactive projectiles
                    for (int i = projectiles.Count - 1; i >= 0; i--)
                        if (!projectiles[i].Active)
                            projectiles.RemoveAt(i);

                    // clean out inactive asteroids
                    for (int i = asteroids.Count - 1; i >= 0; i--)
                        if (!asteroids[i].Active)
                            asteroids.RemoveAt(i);

                    // clean out inactive explosions
                    for (int i = explosions.Count - 1; i >= 0; i--)
                        if (!explosions[i].Animations[EntityState.Idle].Playing || !explosions[i].Active)
                            explosions.RemoveAt(i);

                    // update health displays
                    character1HealthMessage.Color = HealthToColor(character1.Health);
                    character1HealthMessage.Text = character1.Health.ToString() + GameConstants.HealthSuffix;
                    if (currentState == GameState.PlayingVersus)
                    {
                        character2HealthMessage.Color = HealthToColor(character2.Health);
                        character2HealthMessage.Text = character2.Health.ToString() + GameConstants.HealthSuffix;
                    }

                    // update score displays
                    character1ScoreMessage.Text = GameConstants.ScorePrefix + character1.Score.ToString();
                    if (currentState == GameState.PlayingVersus)
                        character2ScoreMessage.Text = GameConstants.ScorePrefix + character2.Score.ToString();

                    // spawn new asteroid if not enough on screen
                    int asteroidRate = (currentState == GameState.PlayingSolo) ? GameConstants.SoloAsteroidSpawnRate : GameConstants.SoloAsteroidSpawnRate;
                    if (asteroids.Count < (int)GameConstants.MinAsteroidCount + currentGameElapsedTime / asteroidRate)
                        AddAsteroid();

                    break;

                default:
                    break;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// Update list of buttons at each update
        /// </summary>
        protected void UpdateDisplayedButtons()
        {
            switch (currentState)
            {
                case GameState.Starting:
                    break;

                case GameState.Menu:
                    // clean buttons
                    menuButtons.Clear();

                    // add appropriate buttons
                    menuButtons.Add(startSoloButton);
                    menuButtons.Add(startVersusButton);
                    menuButtons.Add(quitButton);

                    break;

                case GameState.PlayingSolo:
                case GameState.PlayingVersus:
                    // clean buttons
                    menuButtons.Clear();

                    break;

                case GameState.PauseSolo:
                    // clean buttons
                    menuButtons.Clear();

                    // add appropriate buttons
                    menuButtons.Add(menuButton);
                    menuButtons.Add(resumeSoloButton);
                    menuButtons.Add(quitButton);

                    break;

                case GameState.PauseVersus:
                    // clean buttons
                    menuButtons.Clear();

                    // add appropriate buttons
                    menuButtons.Add(menuButton);
                    menuButtons.Add(resumeVersusButton);
                    menuButtons.Add(quitButton);

                    break;

                case GameState.FinishedSolo:
                    // clean buttons
                    menuButtons.Clear();

                    // add appropriate buttons
                    menuButtons.Add(menuButton);
                    menuButtons.Add(continueSoloButton);
                    menuButtons.Add(quitButton);

                    break;

                case GameState.FinishedVersus:
                    // clean buttons
                    menuButtons.Clear();

                    // add appropriate buttons
                    menuButtons.Add(menuButton);
                    menuButtons.Add(continueVersusButton);
                    menuButtons.Add(quitButton);

                    break;

                default:
                    break;
            }
        }
        /// <summary>
        /// Update list of messages at each update
        /// </summary>
        protected void UpdateDisplayedMessages()
        {
            switch (currentState)
            {
                case GameState.Starting:
                    break;

                case GameState.Menu:
                    // clean messages
                    messages.Clear();
                    character1.ClearPopUps();
                    character2.ClearPopUps();

                    break;

                case GameState.ResetSolo:
                    // clean messages
                    messages.Clear();
                    character1.ClearPopUps();
                    character2.ClearPopUps();

                    // add appropriate messages
                    messages.Add(character1HealthMessage);
                    messages.Add(character1ScoreMessage);

                    break;

                case GameState.ResetVersus:
                    // clean messages
                    messages.Clear();
                    character1.ClearPopUps();
                    character2.ClearPopUps();

                    // add appropriate messages
                    messages.Add(character1HealthMessage);
                    messages.Add(character1ScoreMessage);
                    messages.Add(character1TotalScoreMessage);
                    messages.Add(character2HealthMessage);
                    messages.Add(character2ScoreMessage);
                    messages.Add(character2TotalScoreMessage);

                    break;

                case GameState.PlayingSolo:
                case GameState.PauseSolo:
                    // clean messages
                    messages.Clear();

                    // add appropriate messages
                    messages.Add(character1HealthMessage);
                    messages.Add(character1ScoreMessage);

                    break;

                case GameState.PlayingVersus:
                case GameState.PauseVersus:
                    // clean messages
                    messages.Clear();

                    // add appropriate messages
                    messages.Add(character1HealthMessage);
                    messages.Add(character1ScoreMessage);
                    messages.Add(character1TotalScoreMessage);
                    messages.Add(character2HealthMessage);
                    messages.Add(character2ScoreMessage);
                    messages.Add(character2TotalScoreMessage);

                    break;

                case GameState.FinishedSolo:
                    // clean messages
                    messages.Clear();
                    character1.ClearPopUps();
                    character2.ClearPopUps();

                    // add appropriate messages
                    messages.Add(character1HealthMessage);
                    messages.Add(character1ScoreMessage);

                    break;

                case GameState.FinishedVersus:
                    // clean messages
                    messages.Clear();
                    character1.ClearPopUps();
                    character2.ClearPopUps();

                    // add appropriate messages
                    messages.Add(character1HealthMessage);
                    messages.Add(character1ScoreMessage);
                    messages.Add(character1TotalScoreMessage);
                    messages.Add(character2HealthMessage);
                    messages.Add(character2ScoreMessage);
                    messages.Add(character2TotalScoreMessage);

                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);
            spriteBatch.Begin();

            // draw background
            spriteBatch.Draw(gameBackground, GameConstants.WindowLocation, Color.White);

            if (currentState == GameState.PlayingSolo || currentState == GameState.PlayingVersus
             || currentState == GameState.PauseSolo || currentState == GameState.PauseVersus)
            {
                // draw projectiles
                foreach (Projectile projectile in projectiles)
                projectile.Draw(spriteBatch);

                // draw asteroids
                foreach (Asteroid asteroid in asteroids)
                    asteroid.Draw(spriteBatch);

                // draw explosions
                foreach (Entity explosion in explosions)
                    explosion.Draw(spriteBatch);

                // draw characters
                character1.Draw(spriteBatch);
                if (currentState == GameState.PlayingVersus || currentState == GameState.PauseVersus)
                    character2.Draw(spriteBatch);
            }

            // draw texts
            foreach (TextDisplay message in messages)
                message.Draw(spriteBatch);

            // draw menu background
            if (currentState != GameState.PlayingSolo && currentState != GameState.PlayingVersus)
                spriteBatch.Draw(menuBackground, GameConstants.WindowLocation, Color.White * 0.6f);

            // draw buttons
            foreach (MenuButton button in menuButtons)
                button.Draw(spriteBatch);

            // draw menu text if in menu
            if (currentState == GameState.Menu)
                menuText.Draw(spriteBatch);

            // draw pause text if game paused
            if (currentState == GameState.PauseSolo || currentState == GameState.PauseVersus)
                pauseText.Draw(spriteBatch);

            // draw finish text if game finished
            if (currentState == GameState.FinishedSolo || currentState == GameState.FinishedVersus)
                finishText.Draw(spriteBatch);

            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Checks the collision between a character and a projectile and does the appropriate operations
        /// </summary>
        /// <param name="character">the character to check for a collision with the projectile</param>
        /// <param name="other">the other character</param>
        /// <param name="projectile">the projectile to check for a collision with the character</param>
        private void CheckCollisionCharacterAndProjectile(Character character, Character other, Projectile projectile)
        {
            if (projectile.CollidesWith(character) && projectile.ShotBy == other)
            {
                character.RemoveHealth(projectile.Damage, font);
                other.AddScore(GameConstants.ScorePerHit, font);
                projectile.Active = false;
            }
        }

        /// <summary>
        /// Convert current health to a color for messages
        /// </summary>
        /// <param name="gameTime">current game time</param>
        private Color HealthToColor(int health)
        {
            if (health > (int)(GameConstants.HighHealthPercentage * GameConstants.CharacterStartHealth))
                return GameConstants.GreenColor;
            else if (health > (int)(GameConstants.MediumHealthPercentage * GameConstants.CharacterStartHealth))
                return GameConstants.OrangeColor;
            return GameConstants.RedColor;
        }

        /// <summary>
        /// Add an explosion at the provided location
        /// </summary>
        /// <param name="location">the location at which the explosion should be added</param>
        private void AddExplosion(Point location)
        {
            Animation boom = new Animation(true, false).AddFrames(34, GameConstants.ExplosionSize, GameConstants.ExplosionSize, 0, 0, 10, 5);
            Entity explosion = new Entity(explosionSprite, location, GameConstants.ExplosionSize, GameConstants.ExplosionSize);
            explosion.AddAnimation(EntityState.Idle, boom);
            explosions.Add(explosion);
        }

        /// <summary>
        /// Add an asteroid at the provided location
        /// </summary>
        private void AddAsteroid()
        {
            Animation rockMoving = new Animation(true, true).AddFrames(64, GameConstants.AsteroidSize, GameConstants.AsteroidSize, 0, 0, 100, 8);
            Asteroid asteroid = new Asteroid(asteroidSprite, GameConstants.AsteroidSize, GameConstants.AsteroidSize);
            asteroid.AddAnimation(EntityState.Moving, rockMoving);
            if (currentState == GameState.PlayingSolo)
                asteroid.Damage = GameConstants.SoloAsteroidDamage;
            if (currentState == GameState.PlayingVersus)
                asteroid.Damage = GameConstants.VersusAsteroidDamage;
            asteroids.Add(asteroid);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Adds the given projectile to the game
        /// </summary>
        /// <param name="projectile">the projectile to add</param>
        public static void AddProjectile(Projectile projectile)
        {
            projectiles.Add(projectile);
        }

        /// <summary>
        /// Changes the state of the game
        /// </summary>
        /// <param name="newState">the new game state</param>
        public static void ChangeState(GameState newState)
        {
            currentState = newState;
        }

        #endregion
    }
}
