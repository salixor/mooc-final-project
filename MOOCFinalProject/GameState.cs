﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOOCFinalProject
{
    /// <summary>
    /// An enumeration for possible game states
    /// </summary>
    public enum GameState
    {
        Starting,
        Menu,
        ResetSolo,
        ResetVersus,
        PlayingSolo,
        PlayingVersus,
        PauseSolo,
        PauseVersus,
        FinishedSolo,
        FinishedVersus,
        Exit
    }
}
