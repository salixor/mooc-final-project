﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOOCFinalProject
{
    /// <summary>
    /// An interactive button
    /// </summary>
    class MenuButton : Entity
    {
        #region Fields

        // mapped game state for that button
        GameState nextState;

        // click processing
        bool clickStarted = false;
        bool buttonReleased = true;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new interactive button
        /// </summary>
        /// <param name="sprite">the already loaded sprite for the button</param>
        /// <param name="x">the x location of the center of the button>
        /// <param name="y">the y location of the center of the button</param>
        /// <param name="width">the width of the button</param>
        /// <param name="height">the height of the button</param>
        /// <param name="nextState">the game this button switches to</param>
        public MenuButton(Texture2D sprite, int x, int y,
            int width, int height, GameState nextState)
            : base(sprite, x, y, width, height, Vector2.Zero, false)
        {
            this.nextState = nextState;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Updates the button to check for a button click
        /// </summary>
        /// <param name="gamepad">the current mouse state</param>
        public void Update(MouseState mouse)
        {
            // current source rectangle stored in newSourceRectangle, to highlight button
            Rectangle newSourceRectangle = SourceRectangle;

            // check for mouse over button
            if (DrawRectangle.Contains(mouse.X, mouse.Y))
            {
                // highlight button
                newSourceRectangle.X = Width;

                // check for click started on button
                if (mouse.LeftButton == ButtonState.Pressed &&
                    buttonReleased)
                {
                    clickStarted = true;
                    buttonReleased = false;
                }
                else if (mouse.LeftButton == ButtonState.Released)
                {
                    buttonReleased = true;

                    // if click finished on button, change game state
                    if (clickStarted)
                    {
                        Game1.ChangeState(nextState);
                        clickStarted = false;
                    }
                }
            }
            else
            {
                newSourceRectangle.X = 0;

                // no clicking on this button
                clickStarted = false;
                buttonReleased = false;
            }

            // update source rectangle
            SourceRectangle = newSourceRectangle;
        }

        #endregion
    }
}